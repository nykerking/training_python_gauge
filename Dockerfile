FROM ubuntu

# Install Java.
RUN apt-get update && apt-get install -q -y \
    openjdk-8-jdk \
    apt-transport-https \
    gnupg2 \
    ca-certificates

# Install gauge
RUN apt-key adv --keyserver hkp://ipv4.pool.sks-keyservers.net:80 --recv-keys 023EDB0B && \
    echo deb https://dl.bintray.com/gauge/gauge-deb stable main | tee -a /etc/apt/sources.list

RUN apt-get update && apt-get install gauge

RUN apt-get update && apt-get install -y libaio1 wget unzip

# Install Oracle Instant Client
WORKDIR /opt/oracle

RUN wget https://download.oracle.com/otn_software/linux/instantclient/instantclient-basiclite-linuxx64.zip && \
    unzip instantclient-basiclite-linuxx64.zip && rm -f instantclient-basiclite-linuxx64.zip && \
    cd /opt/oracle/instantclient* && rm -f *jdbc* *occi* *mysql* *README *jar uidrvci genezi adrci && \
    echo /opt/oracle/instantclient* > /etc/ld.so.conf.d/oracle-instantclient.conf && ldconfig

# Install gauge plugins
RUN gauge install java && \
    gauge install screenshot

# Python package dependencies
RUN apt install python3.9 -y
RUN apt install python3-pip -y
RUN pip3 install --upgrade pip
RUN pip3 install colorama
COPY requirements.txt requirements.txt
RUN pip3 install -r requirements.txt

# Linking
RUN ln -s /usr/bin/python3 /usr/bin/python & \
    ln -s /usr/bin/pip3 /usr/bin/pip

# Checking gauge installation
RUN gauge -v

ENV PATH=$HOME/.gauge:$PATH
