# Meister API

## Create 3 tasks
* Get "persons"
* Store the "id" of "person" with "email" "nykertest@gmail.com"
* Get "projects"
* Store the "id" of "project" with "name" "API Training Sandbox"
* Get "sections" of the said "project"
* Store the "id" of "section" with "name" "Open"
* Create "3" "open" task(s) assigned to the said person in the said section
* Store for "expected" assertion the said created task names
* Get "tasks" of the said "section"
* Store for "actual" assertion the said extracted task names from the said section
* The said task names should be listed in the said section

## Accomplish all tasks
* Get "persons"
* Store the "id" of "person" with "email" "nykertest@gmail.com"
* Get "projects"
* Store the "id" of "project" with "name" "API Training Sandbox"
* Get "sections" of the said "project"
* Store the "id" of "section" with "name" "Open"
* Get "tasks", from the said "section", with the following values:
| param_key | param_type | param_value | param_desc |
|-----------|------------|-------------|------------|
| status    | int        | 1           | open       |
* Store for "expected" assertion the said extracted task names from the said section
* Store the id of the said extracted tasks with assigned_to_id of the said person's id
* Store the "id" of "section" with "name" "Done"
* Update the said tasks' section and the following values:
| param_key | param_type | param_value | param_desc |
|-----------|------------|-------------|------------|
| status    | int        | 2           | completed  |
* Get "tasks" of the said "section"
* Store for "actual" assertion the said extracted task names from the said section
* The said task names should be listed in the said section
