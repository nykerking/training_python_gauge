# Data Store

## Age Classification

| birthday   | age_classification |
|------------|--------------------|
| 1970-01-01 | adult              |
| 2020-12-31 | minor              |
| 1950-01-01 | senior             |

* Get age from birthday <birthday>
* Age classification should be <age_classification>

## Age Classification with Continue on Failure

| birthday   | age_classification |
|------------|--------------------|
| 1970-01-01 | adult              |
| not a date | minor              |
| 1950-01-01 | senior             |

* Get age from birthday <birthday>
* Age classification should be <age_classification>
