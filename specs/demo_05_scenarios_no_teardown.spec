# Soft Assertion without Teardown

## Scenario 1
* Scenario 1, step number 1: No problem here
* Scenario 1, step number 2: KeyError Here

## Scenario 2 (Impact)
This test should have passed but nope; failed tests carried over from Scenario 1.
* Scenario 2, step number 1: I should pass
