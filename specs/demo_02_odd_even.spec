# DDT

## Odd-Even Step Parameter
* Number "200022" should yield "even"

## Odd-Even Step Inline Table
* Numbers should yield correct result
| number | result  |
|--------|---------|
| 2      | even    |
| adfs   | neither |

## Odd-Even Step External Table
* Numbers should yield correct result <table: step_impl/inputs/demo_02_odd_even_test_data.csv>

## Odd-Even Scenario Table
Set allow_scenario_datatable variable to true in /env/default/default.properties to enable this table-driven scenario.

| number | result  |
|--------|---------|
| 444    | even    |
| hfgd   | neither |

* Number <number> should yield <result>
