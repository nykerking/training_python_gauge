# Meister API

## Create 3 tasks
* Get "persons"
* Store the "id" of "person" with "email" "nykertest@gmail.com"
* Get "projects"
* Store the "id" of "project" with "name" "API Training Sandbox"
* Get "sections" of the said "project"
* Store the "id" of "section" with "name" "Open"
* Create "3" "open" task(s) assigned to the said person in the said section
* Store for "expected" assertion the said created task names
* Get "tasks" of the said "section"
* Store for "actual" assertion the said extracted task names from the said section
* The said task names should be listed in the said section
