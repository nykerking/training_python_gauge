# Soft Assertion with Teardown

## Scenario 1
* Scenario 1, step number 1: No problem here
* Scenario 1, step number 2: KeyError Here

## Scenario 2 (Impact)
* Scenario 2, step number 1: I should pass

___
* The magical teardown for all scenarios' assertion closure
