# Soft Assertion

## Check List with Continue on Failure Only
* All list elements should be positive numbers (continue on failure only)
* All list elements should be positive numbers (continue on failure only)
* All list elements should be positive numbers (continue on failure only)

## Check List with Soft Assertion Only
* All list elements should be positive numbers (soft assertion only)
* All list elements should be positive numbers (soft assertion only)
* All list elements should be positive numbers (soft assertion only)

## Check List with both Continue on Failure and Soft Assertion
* All list elements should be positive numbers (both continue on failure and soft assertion)
* All list elements should be positive numbers (both continue on failure and soft assertion)
* All list elements should be positive numbers (both continue on failure and soft assertion)
