# DDT

## Triangle Area Step Parameter
* Triangle with base "45.00" cm and height "3.00" cm should have an area of "67.5" cm².

## Triangle Area Step Inline Table
* Triangle area should be correct.
| base  | height | area   |
|-------|--------|--------|
| 3.25  | 9.75   | 15.84  |
| 3.45  | 9.75   | 16.82  |

## Triangle Area Step External Table
* Triangle area should be correct. <table: step_impl/inputs/ex_02_triangle_area_test_data.csv>

## Triangle Area Scenario Table
Set allow_scenario_datatable variable to true in /env/default/default.properties to enable this table-driven scenario.

| base  | height | area  |
|-------|--------|-------|
| 10.57 | 8.60   | 45.45 |
| 2.04  | 9.99   | 10.19 |

* Triangle with base <base> cm and height <height> cm should have an area of <area> cm².
