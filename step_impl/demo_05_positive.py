from getgauge.python import step, data_store, continue_on_failure
from smart_assertions import soft_assert, verify_expectations

listahan = [3465, 8939, -124, 230, -410, -2395, 99]

@continue_on_failure([])
@step("All list elements should be positive numbers (continue on failure only)")
def list_elements_positive_continue_on_failure():
    for element in listahan:
        assert element > 0, "Element {} is not greater than 0".format(element)

@step("All list elements should be positive numbers (soft assertion only)")
def list_elements_positive_soft_assertion():
    for element in listahan:
        soft_assert(
            element > 0, 
            "Element {} is not greater than 0"
            .format(element)
        )
    verify_expectations()

@continue_on_failure([])
@step("All list elements should be positive numbers (both continue on failure and soft assertion)")
def list_elements_positive_both():
    for element in listahan:
        soft_assert(
            element > 0, 
            "Element {} is not greater than 0"
            .format(element)
        )
    verify_expectations()
