from getgauge.python import step
from decimal import Decimal

@step("Triangle with base <base> cm and height <height> cm should have an area of <area> cm².")
def assert_triangle_area_param(base, height, area):
    print("base: {} cm | height: {} cm | area: {} cm".format(base, height, area))
    expected_area = round(Decimal(area), 2)
    actual_area = round(Decimal(base) * Decimal(height) / 2, 2)
    assert expected_area == actual_area , "Expected triangle area: {} cm² vs. Actual triangle area: {} cm²".format(expected_area, actual_area)

@step("Triangle area should be correct. <table>")
def assert_triangle_area_table_inline(table):
    for base, height, area in zip(table.get_column_values_with_name("base"), table.get_column_values_with_name("height"), table.get_column_values_with_name("area")):
        assert_triangle_area_param(base, height, area)
