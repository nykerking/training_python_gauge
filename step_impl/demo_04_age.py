from getgauge.python import step, data_store, continue_on_failure
from datetime import datetime

# @continue_on_failure([ValueError])
@step("Get age from birthday <birthday>")
def get_age_from_birthday(birthday):
    data_store.scenario.age = ( datetime.today() - datetime.strptime(birthday, "%Y-%m-%d") ).days // 365

@step("Age classification should be <age_classification>")
def assert_odd_even_table_inline(age_classification):
    if 0 <= data_store.scenario.age < 18:
        assert age_classification.lower() == "minor"
    elif 18 <= data_store.scenario.age <= 65:
        assert age_classification.lower() == "adult"
    elif 65 < data_store.scenario.age:
        assert age_classification.lower() == "senior"
