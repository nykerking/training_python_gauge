from getgauge.python import step, data_store
from smart_assertions import soft_assert, verify_expectations
import jsonpath_rw_ext as jsonpath
import requests
import step_impl.defaults.credentials as credentials
import time

@step("Get persons")
def get_persons():
    request_header = {
        "Authorization": "Bearer " + credentials.meister_access_token
    }
    response = requests.request(
        method="GET", 
        url="https://www.meistertask.com/api/persons",
        headers=request_header
    )
    # print(response.json())
    data_store.scenario['response_persons'] = response

@step("Get user id")
def get_user_id():
    data_store.scenario['my_user_id'] = ( jsonpath.match("$[?(@.email=='nykertest@gmail.com')].id", data_store.scenario['response_persons'].json()) )[0]
    # print(data_store.scenario['my_user_id'])

@step("Get projects")
def get_persons():
    request_header = {
        "Authorization": "Bearer " + credentials.meister_access_token
    }
    response = requests.request(
        method="GET", 
        url="https://www.meistertask.com/api/projects",
        headers=request_header
    )
    # print(response.json())
    data_store.scenario['response_projects'] = response

@step("Get project id")
def get_project_id():
    data_store.scenario['project_id'] = ( jsonpath.match("$[?(@.name=='API Training Sandbox')].id", data_store.scenario['response_projects'].json()) [0])
    # print(data_store.scenario['project_id'])

@step("Get sections")
def get_sections():
    request_header = {
        "Authorization": "Bearer " + credentials.meister_access_token
    }
    response = requests.request(
        method="GET", 
        url="https://www.meistertask.com/api/projects/" + str(data_store.scenario['project_id']) + "/sections",
        headers=request_header
    )
    # print(response.json())
    data_store.scenario['response_sections'] = response

@step("Get section id")
def get_section_id():
    data_store.scenario['section_id'] = ( jsonpath.match("$[?(@.name=='Open')].id", data_store.scenario['response_sections'].json()) [0])
    # print(data_store.scenario['section_id'])

@step("Create task")
def create_task():
    request_header = {
        "Authorization": "Bearer " + credentials.meister_access_token
    }
    data_store.scenario['response_create_task'] = []
    for ctr in range(3):
        current_timestamp = str(int(time.time()*1000))
        request_body = {
            "name": "nyker+" + current_timestamp,
            "assigned_to_id": str(data_store.scenario['my_user_id']),
            "status": 1
        }
        response = requests.request(
            method="POST", 
            url="https://www.meistertask.com/api/sections/" + str(data_store.scenario['section_id']) + "/tasks",
            headers=request_header,
            data=request_body
        )
        # print(response.json())
        data_store.scenario['response_create_task'].append(response)

@step("Get tasks")
def get_tasks():
    request_header = {
        "Authorization": "Bearer " + credentials.meister_access_token
    }
    request_params = {
        "items": 10000,
        "page": 1
    }
    response = requests.request(
        method="GET", 
        url="https://www.meistertask.com/api/sections/" + str(data_store.scenario['section_id']) + "/tasks",
        headers=request_header,
        params=request_params
    )
    # print(response.json())
    data_store.scenario['response_tasks'] = response

@step("Assert tasks were created")
def assert_tasks_were_created():
    for expected_task in data_store.scenario['response_create_task']:
        soft_assert(
            len(jsonpath.match("$[?(@.name=='" + expected_task.json()['name'] + "')]", data_store.scenario['response_tasks'].json())) == 1,
            "Task: {} is not found in data_store.scenario['response_tasks'].json(): {}"
            .format(expected_task.json()['name'], data_store.scenario['response_tasks'].json())
        )
    verify_expectations()
