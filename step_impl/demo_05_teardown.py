from getgauge.python import continue_on_failure, data_store, step
from smart_assertions import soft_assert, verify_expectations

@continue_on_failure([])
@step("Scenario 1, step number 1: No problem here")
def step_number_1():
    soft_assert(
        True is True,
        "True is not True"
    )
    verify_expectations()

@continue_on_failure([])
@step("Scenario 1, step number 2: KeyError Here")
def step_number_2():
    soft_assert(
        True is False,
        "I'm from Scenario 1, step number 2. Yeah this test should fail. But I should not haunt you on your next immediate scenario."
    )
    soft_assert(
        data_store.scenario['kabute_key_biglang_sulpot'],
        "This message won't be seen in the report because of KeyError"
    )
    verify_expectations()

@continue_on_failure([])
@step("Scenario 2, step number 1: I should pass")
def step_number_3():
    soft_assert(
        "a" == "a",
        "a != a"
    )
    verify_expectations()

@step("The magical teardown for all scenarios' assertion closure")
def assertion_closure():
    verify_expectations()
