from getgauge.python import Messages
import time

def execute_query(conn, fetch_no_of_retries, fetch_duration_interval_in_sec, query, minimum_expected_record_count):
    Messages.write_message("SQL TO RUN: \n" + query)
    print("SQL TO RUN: \n" + query)
    column_names = []
    records = []
    with conn.cursor() as cursor:
        for no_of_tries in range(fetch_no_of_retries):
            cursor.execute(query)
            records = cursor.fetchall()
            print("Query executed {} time(s). Expected record(s) count: {} vs. Actual record(s) found: {}".format(no_of_tries + 1, minimum_expected_record_count, len(records)))
            if( len(records) < minimum_expected_record_count ):
                print("Sleeping for " + str(fetch_duration_interval_in_sec) + " seconds...")
                time.sleep(fetch_duration_interval_in_sec)
            else:
                break
        else:
            if( len(records) < minimum_expected_record_count ):
                Messages.write_message("Waited for {} seconds but only {} records were found.".format( ((no_of_tries + 1) * fetch_duration_interval_in_sec), len(records)) )
                print("Waited for {} seconds but only {} records were found.".format( ((no_of_tries + 1) * fetch_duration_interval_in_sec), len(records)) )
        column_names = [desc[0] for desc in cursor.description]
    # print("Column names: \n" + column_names)
    # print("Records: \n" + records)
    formatted_records = []
    for record in records:
        formatted_records.append( dict((column_name, record_cell) for column_name, record_cell in zip(column_names, record)) )
    Messages.write_message("formatted_records: \n" + str(formatted_records))
    return formatted_records

def execute_query_and_wait_cell_not_none(conn, fetch_no_of_retries, fetch_duration_interval_in_sec, query, minimum_expected_record_count, column_name, record_zero_base_index=0):
    cell = None
    for column_no_of_retries in range(fetch_no_of_retries):
        formatted_records = execute_query(conn, fetch_no_of_retries, fetch_duration_interval_in_sec, query, minimum_expected_record_count)
        cell = formatted_records[record_zero_base_index][column_name]
        print("Requery executed {} time(s). Actual {}th {}'s value found: {}".format(column_no_of_retries + 1, record_zero_base_index, column_name, cell))
        if(not(cell)):
            print("Sleeping for " + str(fetch_duration_interval_in_sec) + " seconds...")
            time.sleep(fetch_duration_interval_in_sec)
        else:
            break    
    else:
        if( cell ):
            print("Waited for {} seconds but {}th {}'s value found was still: {}".format( ((column_no_of_retries + 1) * fetch_duration_interval_in_sec), record_zero_base_index, column_name, cell) )
            Messages.write_message("Waited for {} seconds but {}th {}'s value found was still: {}".format( ((column_no_of_retries + 1) * fetch_duration_interval_in_sec), record_zero_base_index, column_name, cell) )
    return formatted_records
