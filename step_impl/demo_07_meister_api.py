from getgauge.python import step, data_store, continue_on_failure, Messages
from smart_assertions import soft_assert, verify_expectations
import copy
import jsonpath_rw_ext as jsonpath
import requests
import step_impl.defaults.config_request as config_request
import step_impl.defaults.credentials as credentials
import time

MAP_TASK_STATUS = {
    "OPEN": 1,
    "COMPLETED": 2,
    "TRASHED": 8,
    "COMPLETED_ARCHIVED": 18
}

@step("Get <target_entities>")
def get_target_entities(target_entities):
    # Describing action
    request_method = "GET"
    action = request_method + " " + target_entities.title()
    Messages.write_message(action)
    print(action)
    endpoint = "/" + target_entities
    Messages.write_message(endpoint)
    print(endpoint)
    # Preparing request header
    request_header = {
        "Authorization": "Bearer " + credentials.meister_access_token
    }
    # Sending request and receiving response
    response = requests.request(
        method=request_method,
        url=config_request.meister_base_url_api + endpoint,
        timeout=config_request.timeout,
        verify=True,
        params={},
        headers=request_header
    )
    # Storing response
    key_action = action.lower().replace(" ", "_")
    if not(key_action in data_store.scenario):
        data_store.scenario[key_action] = {}
    data_store.scenario[key_action]['response'] = response
    # Describing response
    Messages.write_message("Actual response body: \n" + response.text)
    print("Actual response body: \n" + response.text)
    try:
        Messages.write_message("Actual response code: " + str(response.status_code))
        print("Actual response code: " + str(response.status_code))
    except:
        pass

@step(["Get <target_entities> of the said <scenario_reference_entity>", "Get <target_entities>, from the said <scenario_reference_entity>, with the following values: <table>"])
def get_tar_entity_of_scenario_ref_entity(target_entities, scenario_reference_entity, table=None):
    # Describing action
    request_method = "GET"
    action = request_method + " " + target_entities.title()
    Messages.write_message(action)
    print(action)
    endpoint = "/" + scenario_reference_entity + "s" + "/" + str(data_store.scenario[scenario_reference_entity]['id']) + "/" + target_entities
    Messages.write_message(endpoint)
    print(endpoint)
    # Preparing request query parameters
    request_query_parameters = {}
    if (table):
        request_query_parameters = {}
        for param_key, param_type, param_value in zip(table.get_column_values_with_name("param_key"), table.get_column_values_with_name("param_type"), table.get_column_values_with_name("param_value")):
            to_exec = "request_query_parameters" + "['" + param_key + "']" + " = " + param_type + "(" + param_value + ")"
            exec(to_exec)
            Messages.write_message("Request query parameters:\n{}".format(request_query_parameters))
    request_query_parameters['items'] = 100000
    # Preparing request header
    request_header = {
        "Authorization": "Bearer " + credentials.meister_access_token
    }
    # Sending request and receiving response
    response = requests.request(
        method=request_method,
        url=config_request.meister_base_url_api + endpoint,
        timeout=config_request.timeout,
        verify=True,
        params=request_query_parameters,
        headers=request_header
    )
    # Storing response
    key_action = action.lower().replace(" ", "_")
    if not(key_action in data_store.scenario):
        data_store.scenario[key_action] = {}
    data_store.scenario[key_action]['response'] = response
    # Describing response
    Messages.write_message("Actual response body: \n" + response.text)
    print("Actual response body: \n" + response.text)
    try:
        Messages.write_message("Actual response code: " + str(response.status_code))
        print("Actual response code: " + str(response.status_code))
    except:
        pass

@step("Store the <target_attribute> of <entity> with <reference_attribute> <reference_attribute_value>")
def store_entity_tar_attr_with_ref_attr_item(target_attribute, entity, reference_attribute, reference_attribute_value):
    jsonpath_query = "$[?(@." + reference_attribute + "=='" + reference_attribute_value + "')]." + target_attribute
    Messages.write_message("JSONPath query:\t{}".format(jsonpath_query))
    key_action = "get" + "_" + entity.lower() + "s"
    jsonpath_query_result = ( jsonpath.match(jsonpath_query, data_store.scenario[key_action]['response'].json()) )[0]
    Messages.write_message("JSONPath query result:\t{}".format(jsonpath_query_result))
    if not(entity.lower() in data_store.scenario):
        data_store.scenario[entity.lower()] = {}
    data_store.scenario[entity.lower()][target_attribute] = jsonpath_query_result
    print("data_store.scenario dictionary content: \n{}".format(data_store.scenario))

@step("Create <quantity> <status> task(s) assigned to the said person in the said section")
def create_tasks(quantity, status):
    # Describing action
    request_method = "POST"
    action = request_method + " " + "Tasks"
    Messages.write_message(action)
    print(action)
    endpoint = "/" + "sections" + "/" + str(data_store.scenario['section']['id']) + "/" + "tasks"
    Messages.write_message(endpoint)
    print(endpoint)
    # Preparing storage for task names
    data_store.scenario.created_task_names = []
    # Preparing storage for responses
    key_action = action.lower().replace(" ", "_")
    if not(key_action in data_store.scenario):
        data_store.scenario[key_action] = {}
    data_store.scenario[key_action]['responses'] = []
    for counter in range(int(quantity)):
        # Preparing request header
        request_header = {
            "Authorization": "Bearer " + credentials.meister_access_token
        }
        # Preparing request body
        current_timestamp = str(int(time.time()*1000))
        task_name = "API task + " + current_timestamp
        request_body = {
            "name": task_name,
            "assigned_to_id": data_store.scenario['person']['id'],
            "due": "",
            "notes": "",
            "status": MAP_TASK_STATUS[status.upper()],
            "label_ids": [
            ],
            "custom_fields": [
            ],
            "checklists": [
            ]
        }
        # Remembering task name
        data_store.scenario.created_task_names.append(task_name)
        # Sending request and receiving response
        response = requests.request(
            method=request_method,
            url=config_request.meister_base_url_api + endpoint,
            timeout=config_request.timeout,
            verify=True,
            params={},
            headers=request_header,
            json=request_body
        )
        # Storing response
        data_store.scenario[key_action]['responses'].append(response)
        # Describing response
        Messages.write_message("Actual response body: \n" + response.text)
        print("Actual response body: \n" + response.text)
        try:
            Messages.write_message("Actual response code: " + str(response.status_code))
            print("Actual response code: " + str(response.status_code))
        except:
            pass

@step("Store for <result_type> assertion the said created task names")
def store_said_created_task_names(result_type):
    if not("result" in data_store.scenario):
        data_store.scenario['result'] = {}
    data_store.scenario['result'][result_type] = copy.deepcopy(data_store.scenario.created_task_names)

@step("Store for <result_type> assertion the said extracted task names from the said section")
def store_said_extracted_task_names_from_the_said_section(result_type):
    if not("result" in data_store.scenario):
        data_store.scenario['result'] = {}
    data_store.scenario['result'][result_type] = jsonpath.match("$[*].name", data_store.scenario['get_tasks']['response'].json())

@continue_on_failure([])
@step("The said task names should be listed in the said section")
def assert_said_task_names_in_said_section():
    soft_assert(
        set(data_store.scenario['result']['expected']).issubset(set(data_store.scenario['result']['actual'])),
        "Said task names: {} are not listed in the said section: {}"
        .format(data_store.scenario['result']['expected'], data_store.scenario['result']['actual'])
    )
    verify_expectations() # assertion to fix

@step("Store the id of the said extracted tasks with assigned_to_id of the said person's id")
def store_said_extracted_task_ids_with_given_person_id():
    Messages.write_message("JSONPath query: {}".format("$[?(@.assigned_to_id==" + str(data_store.scenario['person']['id']) + ")].id"))
    print("JSONPath query: {}".format("$[?(@.assigned_to_id==" + str(data_store.scenario['person']['id']) + ")].id"))
    data_store.scenario['task_ids'] = jsonpath.match("$[?(@.assigned_to_id==" + str(data_store.scenario['person']['id']) + ")].id", data_store.scenario['get_tasks']['response'].json())
    Messages.write_message("JSONPath query results: {}".format(data_store.scenario['task_ids']))
    print("JSONPath query results: {}".format(data_store.scenario['task_ids']))

@step("Update the said tasks' section and the following values: <table>")
def update_said_tasks_to_said_section(table):
    # Describing action
    request_method = "PUT"
    action = request_method + " " + "Tasks"
    Messages.write_message(action)
    print(action)
    # Preparing storage for responses
    key_action = action.lower().replace(" ", "_")
    if not(key_action in data_store.scenario):
        data_store.scenario[key_action] = {}
    data_store.scenario[key_action]['responses'] = []
    for task_id in data_store.scenario['task_ids']:
        # Describing action (continued)
        endpoint = "/" + "tasks" + "/" + str(task_id)
        Messages.write_message(endpoint)
        print(endpoint)
        # Preparing request header
        request_header = {
            "Authorization": "Bearer " + credentials.meister_access_token
        }
        request_body = {
            "section_id": data_store.scenario['section']['id']
        }
        if (table):
            for param_key, param_type, param_value in zip(table.get_column_values_with_name("param_key"), table.get_column_values_with_name("param_type"), table.get_column_values_with_name("param_value")):
                to_exec = "request_body" + "['" + param_key + "']" + " = " + param_type + "(" + param_value + ")"
                exec(to_exec)
                Messages.write_message("Request body:\n{}".format(request_body))
        # Sending request and receiving response
        response = requests.request(
            method=request_method,
            url=config_request.meister_base_url_api + endpoint,
            timeout=config_request.timeout,
            verify=True,
            params={},
            headers=request_header,
            json=request_body
        )
        # Storing response
        data_store.scenario[key_action]['responses'].append(response)
        # Describing response
        Messages.write_message("Actual response body: \n" + response.text)
        print("Actual response body: \n" + response.text)
        try:
            Messages.write_message("Actual response code: " + str(response.status_code))
            print("Actual response code: " + str(response.status_code))
        except:
            pass
