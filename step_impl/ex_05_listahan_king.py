from getgauge.python import step, data_store, continue_on_failure
from smart_assertions import soft_assert, verify_expectations

grocery_list = ["vegetable", "fruit", "chicken", "chip", "milk"]
word_list = ["answer", "rhythm", "crypt", "cyst", "gyms", "jynx"]
todo_list = ["plan", "develop", "test", "deploy"]

@continue_on_failure([])
@step("List of grocery items should not contain <grocery_item>")
def list_elements_positive_continue_on_failure(grocery_item):
    for grocery_item in grocery_list:
        soft_assert(
            grocery_item != "chip",
            "{} should not be found in grocery list: {}"
            .format(grocery_item, grocery_list)
        )
    verify_expectations()

@continue_on_failure([])
@step("List of words should not contain vowels <vowels>")
def list_elements_positive_continue_on_failure(vowels):
    for word in word_list:
        for vowel in vowels:
            soft_assert(
                not(vowel in word),
                "{} should not contain a vowel but vowel, {}, was found."
                .format(word, vowel)
            )
    verify_expectations()

@continue_on_failure([])
@step("List of todo should not contain <todo_item>")
def list_elements_positive_continue_on_failure(todo_item):
    for todo_item in todo_list:
        soft_assert(
            todo_item != "debug",
            "{} should not be found in ToDo list: {}"
            .format(todo_item, todo_list)
        )
    verify_expectations()
