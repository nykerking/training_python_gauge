meister_base_url_protocol_api = "https"
meister_base_domain_api = "www.meistertask.com"
meister_api_specific_endpoint = "/api"
meister_api_version = ""
meister_base_url_api = meister_base_url_protocol_api \
                        + "://" \
                        + meister_base_domain_api \
                        + meister_api_specific_endpoint \
                        + meister_api_version

timeout = 5.0
