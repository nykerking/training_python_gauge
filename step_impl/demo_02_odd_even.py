from getgauge.python import step

@step("Number <number> should yield <result>")
def assert_odd_even_param(number, result):
    print("number: {} | result: {}".format(number, result))
    if not(number.isnumeric()):
        assert result.lower() == "neither"
    elif int(number) % 2 == 0:
        assert result.lower() == "even"
    elif int(number) % 2 == 1:
        assert result.lower() == "odd"

@step("Numbers should yield correct result <table>")
def assert_odd_even_table_inline(table):
    for number, result in zip(table.get_column_values_with_name("number"), table.get_column_values_with_name("result")):
        assert_odd_even_param(number, result)
